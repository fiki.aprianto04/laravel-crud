<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function regis()
    {
        return view('halaman.regis');
    }

    public function welcome(Request $request)
    {
       // dd($request->all());
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $Gender = $request['Gender'];
        $Nationality = $request['Nationality'];
        $Language = $request['Language'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('firstname','lastname','Gender','Nationality','Language','bio'));
    }
    //
}
